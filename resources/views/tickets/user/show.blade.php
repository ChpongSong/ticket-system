@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a class="btn-link" href="/tickets/{{ $tickets->id }}/edit" target="_blank">Редактирование тикета</a>
        </p>
        <br>
        <h4 class="title">
            {{ $tickets->title }}
        </h4>
        <div class="content">

            <p>
                {{ $tickets->description }}
            </p>

        </div>

        @foreach($ticketMessages as $message)
            <table class="table">
                <thead>
                <tr>
                    <th>
                        {{ $message->user->name }}
                    </th>
                    <th>
                        {{ $message->user_role }}
                    </th>
                    <th>
                        {{ $message->created_at }}
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $message->message }}</td>
                </tr>
                </tbody>
            </table>
        @endforeach

        @if($tickets->status === 'open')
            <form action="/tickets/{{ $tickets->id }}" method="POST">
                @csrf

                <div class="field">
                    <div class="form-group">
                        <label for="message" class="label">Message</label>
                        <input type="text" class="form-control" id="message" name="message" value=""
                               placeholder="Отправить сообщение">
                    </div>
                </div>

                <div class="field">
                    <div class="control">
                        <button type="submit" class="btn btn-link">Отправка сообщения</button>
                    </div>
                </div>
            </form>
        @endif
    </div>

@endsection
