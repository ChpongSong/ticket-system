<thead>
<tr>
    <th>
        {{ $message['user_role'] }}
    </th>
    <th>
        {{ $message['created_at'] }}
    </th>
</tr>
</thead>
<tbody>
<tr>
    <td>
        {{ $message['message'] }}
    </td>
</tr>
</tbody>
