@extends('layouts/app')

@section('content')
    <div class="container">
        <a href="/tickets" class="btn btn-primary">Главная</a>

        <div class="container" style="text-align: center">
            <h1>Создание нового тикета</h1>
        </div>

        <form method="POST" action="/tickets">
            @csrf
            <div class="form-group">
                <label for="title">Заголовок</label>
                <input type="text" class="form-control " id="title"
                       name="title" value="{{ old('title') }}">
            </div>

            <div class="form-group">
                <label for="comment">Описание проблемы</label>
                <textarea class="form-control " rows="5"
                          id="comment" name="description">{{ old('description') }}</textarea>
            </div>

            <div class="form-group">
                <label for="selectDepartments">Выбор Отдела</label>
                <select class="form-control" id="selectDepartments" name="department_id">
                    @foreach($departments as $department)
                        <option name="department_id" value="{{$department['id']}}">{{$department['name']}}</option>
                    @endforeach
                </select>
            </div>

            <div>
                <button type="submit" class="btn btn-primary">Отправить</button>
            </div>

        </form>
    </div>
@endsection
