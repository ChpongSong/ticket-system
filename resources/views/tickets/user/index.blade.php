@extends('layouts/app')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header" style="text-align: center">
                Просмотр всех тикетов
            </div>
            <div class="car-body" style="text-align: end">

                <form action="/tickets" method="GET">
                    Показать закрытые тикеты
                    <input type="checkbox" onchange="this.form.submit()" value="close" name="status">
                </form>

                <form action="/tickets" method="GET">
                    Показать только открытые тикеты
                    <input type="checkbox" onchange="this.form.submit()" value="open" name="status">
                </form>
                @if($auth->user_role === 'user')
                    <div class="btn-group">
                        <a href="/tickets/create" class="btn-link">Создание тикета </a>
                    </div>
                @endif
                <div class="dropdown open">
                    <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Выбор отдела
                    </a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <form action="/tickets" method="GET">
                            @foreach($departments as $department)
                                {{ $department->name }}
                                <input type="checkbox" name="department_id" onchange="this.form.submit()"
                                       value="{{$department->id}}">
                                <br>
                            @endforeach
                        </form>
                    </div>
                </div>

                <br>

            </div>

            <div class="container">
                @foreach($tickets as $ticket)
                    <table class="table">
                        <thead class="table-secondary">
                        <tr>
                            <th>
                                creater : <a href="/tickets/{{$ticket->id}}/show" target="_blank">
                                    {{ $ticket->user->name }}
                                </a>

                            </th>
                            <th>
                                {{ $ticket->department->name }}/{{ $ticket->created_at }}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h3>
                                    {{ $ticket->title }}
                                </h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    {{ $ticket->description }}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                @php  $message = \App\Models\TicketMessages::query()->where('ticket_id', $ticket->id)->get()->last() @endphp
                                @include('tickets.user.chat.last-m',  [$message])
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div>
                    </div>
                    <br>
                    @if($ticket->status == 'close')
                        <p class="btn-success">Тикет закрыт</p>
                    @endif
                    <br>

                    @if($ticket->status == 'open' && $auth->user_role == 'moderator')
                        <form action="/tickets/{{ $ticket->id }}" method="POST">
                            @method('DELETE')
                            @csrf

                            <button type="submit" class="bg-warning">
                                Закрыть тикет
                            </button>
                        </form>
                    @endif
                    <br>
                @endforeach
            </div>

            {{--            <div class="container">--}}
            {{--                @if($tickets->total() > $tickets->count())--}}
            {{--                    <br>--}}
            {{--                    <div class="row justify-content-center">--}}
            {{--                        <div class="col-md-12">--}}
            {{--                            <div class="card">--}}
            {{--                                <div class="card-body">--}}
            {{--                                    {{ $tickets->links() }}--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                @endif--}}
            {{--            </div>--}}

        </div>

    </div>
@endsection
