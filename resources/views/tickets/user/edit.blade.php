@extends('layouts.app')

@section('content')

    <div class="container">
        <h1 class="card-title">Редактирование Тикета</h1>

        <form method="POST" action="/tickets/{{ $tickets->id }}">
            @method('PATCH')
            @csrf

            <div class="field">
                <div class="form-group">
                    <label for="title" class="label">Заголовок</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ $tickets->title }}">
                </div>
            </div>

            <div class="field">
                <div class="form-group">
                    <label for="description" class="label">Описание</label>
                    <textarea class="form-control" rows="5" id="comment"
                              name="description">{{ $tickets->description }}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="selectDepartments">Выбор Отдела</label>
                <select class="form-control" id="selectDepartments" name="department_id">
                    @foreach($departments as $department)
                        <option name="department_id" value="{{$department['id']}}">{{$department['name']}}</option>
                    @endforeach
                </select>
            </div>

            <div class="field">
                <div class="control">
                    <button type="submit" class="btn btn-link">Обновить тикет</button>
                </div>
            </div>
        </form>

    </div>
@endsection
