<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserTicket;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Check User Role
     *
     * @param User $user
     * @return bool
     */
    public function checkUserRole(User $user)
    {
        return $user->user_role === User::DefaultUser;
    }

    /**
     * Check Moderator role
     *
     * @param User $user
     * @return bool
     */
    public function checkModeratorRole(User $user)
    {
        return $user->user_role === User::ModeratorUser;
    }
}
