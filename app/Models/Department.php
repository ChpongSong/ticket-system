<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    const IT_DEPARTMENT = 'IT отдел';
    const SALES_DEPARTMENT = 'Отдел продаж';
    const FINANCE_DEPARTMENT = 'Финансовый отдел';

    /**
     * @return array
     */
    public static function getDepartments()
    {
        return [
            self::IT_DEPARTMENT,
            self::SALES_DEPARTMENT,
            self::FINANCE_DEPARTMENT
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(UserTicket::class);
    }

}
