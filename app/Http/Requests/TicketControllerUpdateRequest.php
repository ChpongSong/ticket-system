<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketControllerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id',
            'department_id',
            'title' => 'required',
            'description' => 'required',
        ];
    }
}
