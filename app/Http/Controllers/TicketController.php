<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketControllerShowTicketsRequest;
use App\Http\Requests\TicketControllerStoreMessagesRequest;
use App\Http\Requests\TicketControllerStoreRequest;
use App\Http\Requests\TicketControllerUpdateRequest;
use App\Models\Department;
use App\Models\TicketMessages;
use App\Models\User;
use App\Models\UserTicket;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    /**
     * @param TicketControllerShowTicketsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getTicketByUserId()
    {
        $auth = Auth::user();

        if ($auth->can('checkUserRole', User::class)) {
            $tickets = $auth->tickets;
            $departments = Department::query()->get(['name', 'id']);
            return view('tickets.user.index', compact('tickets', 'departments', 'auth'));
        }
        if ($auth->can('checkModeratorRole', User::class)) {
            $tickets = UserTicket::query()->select([
                'id',
                'user_id',
                'department_id',
                'title',
                'description',
                'created_at',
                'status'
            ])->get();
            $departments = Department::query()->get(['name', 'id']);
            return view('tickets.user.index', compact('tickets', 'departments', 'auth'));
        }
        return redirect('/login');
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $auth = Auth::user();
        if ($auth->user_role == User::DefaultUser && UserTicket::query()->where([
                'user_id' => $auth->id,
                'id' => $id
            ])->first()) {
            $tickets = UserTicket::query()->where('id', $id)->first();
            $departments = Department::query()->orderBy('id', 'desc')->get(['name', 'id'])->toArray();
            return view('tickets.user.edit', compact('tickets', 'departments'));
        }
        return redirect('/tickets');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $auth = Auth::user();

        if ($auth->can('checkUserRole', User::class)) {
            $tickets = UserTicket::query()->where(['user_id' => $auth->id, 'id' => $id])->first();
            $ticketMessages = TicketMessages::query()->where('ticket_id', $id)->get();
            return view('tickets.user.show', compact('tickets', 'ticketMessages'));
        }
        if ($auth->can('checkModeratorRole', User::class)) {
            $tickets = UserTicket::query()->where('id', $id)->first();
            $ticketMessages = TicketMessages::query()->where('ticket_id', $id)->get();
            return view('tickets.user.show', compact('tickets', 'ticketMessages'));
        }
        return redirect('/tickets');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function create()
    {
        $auth = Auth::user();

        if ($auth->can('checkUserRole', User::class)) {
            $departments = Department::query()->get(['name', 'id']);
            return view('tickets.user.create', compact('departments'));
        }
        return redirect('/tickets');
    }

    /**
     * @param TicketControllerStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(TicketControllerStoreRequest $request)
    {
        $auth = Auth::user();
        if ($auth->can('checkUserRole', User::DefaultUser)) {
            UserTicket::create([
                'user_id' => $auth->id,
                'department_id' => $request->get('department_id'),
                'title' => $request->get('title'),
                'description' => $request->get('description')
            ]);
            return redirect('/tickets');

        }
        return redirect('/tickets');
    }

    /**
     * @param TicketControllerUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(TicketControllerUpdateRequest $request, $id)
    {
        $auth = Auth::user();

        if ($auth->can('checkUserRole', User::class)) {
            UserTicket::query()->where('id', $id)->update([
                'user_id' => $auth->id,
                'department_id' => $request->get('department_id'),
                'title' => $request->get('title'),
                'description' => $request->get('description')
            ]);
            return back();
        }
        return redirect('/tickets');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        $auth = Auth::user();
        if ($auth->can('checkModeratorRole', User::class)) {
            UserTicket::query()->where('id', $id)->update(['status' => UserTicket::STATUS_CLOSE]);
            return redirect('/tickets');
        }
        return redirect('/tickets');
    }

    /**
     * @param TicketControllerStoreMessagesRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeMessages(TicketControllerStoreMessagesRequest $request, $id)
    {
        $auth = Auth::user();

        if ($auth->can('checkUserRole', User::class)) {
            TicketMessages::create([
                'user_id' => $auth->id,
                'ticket_id' => $id,
                'user_role' => $auth->user_role,
                'message' => $request->get('message')
            ]);

            return back();
        }
        if ($auth->can('checkModeratorRole', User::class)) {

            TicketMessages::create([
                'user_id' => $auth->id,
                'ticket_id' => $id,
                'user_role' => $auth->user_role,
                'message' => $request->get('message')
            ]);

            return back();
        }
        return redirect('/tickets');
    }

}
