<?php

use App\Models\User;
use App\Models\UserTicket;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tickets = [];
        $faker = Faker::create();
        $usersId = User::query()
            ->where('user_role', User::DefaultUser)
            ->pluck('id')
            ->toArray();

        for ($ticket = 1; $ticket < 50; $ticket++) {
            $tickets[$ticket]['user_id'] = array_rand($usersId);
            $tickets[$ticket]['department_id'] = rand(1, 3);
            $tickets[$ticket]['title'] = $faker->text;
            $tickets[$ticket]['description'] = $faker->realText(rand(20, 50));
            $tickets[$ticket]['status'] = UserTicket::STATUS_OPEN;
        }

        UserTicket::query()->insert($tickets);
    }
}
