<?php

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = Department::getDepartments();
        foreach ($departments as $key => $department) {
            Department::query()->insert(['name' => $department]);
        }
    }
}
