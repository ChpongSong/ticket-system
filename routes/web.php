<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect()->route('tickets');
});

Route::group(['prefix' => 'tickets', 'middleware' => ['auth']], function () {
    Route::get('/', 'TicketController@getTicketByUserId')->name('tickets');
    Route::get('/create', 'TicketController@create');
    Route::post('/', 'TicketController@store');
    Route::get('/{ticket}/show', 'TicketController@show');
    Route::get('/{id}/edit', 'TicketController@edit');
    Route::patch('/{id}', 'TicketController@update');
    Route::delete('/{id}', 'TicketController@delete');
    Route::post('/{id}', 'TicketController@storeMessages');
});
